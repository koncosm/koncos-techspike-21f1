## Goal

The goal of this project was to familiarize with the Angular framework and the Typescript language. Furthermore, this project was used to learn about file upload and saving files on a simple JSON object backend. This knowledge will be used to implement the submission and graded submission download feature for the OpComp Web Application.

## Technologies Tried and Used

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.5. HTML and CSS were used for editing the frontend style and organization.

## Building and Running the Project

In a terminal within the project folder:

1. Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
2. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
3. Run `npm run server` on a separate terminal for the simple backend. If you want to see the backend contents (tasks), navigate to `http://localhost:5000/`

## Testing the project

1. Build and run the project
2. Feel free to add tasks manually by following directions and explore the webpage through the 'About' page
3. Also, to test the upload feature, choose a text file with a properly formatted JSON object representing the task. There is currently no file content validation so if an incorrectly formatted text file is uploaded, the functionality will not work.

## Lessons Learned

Throughout exploring the Angular framework, I've found out about its main advantages and drawbacks. Some of the main advantages of the framework are its robustness and ability to modify an immense amount of details. The robustness of Angular also seems to be its main drawback as creating simple features takes up many files and an unnecessary amount of code. The file upload feature is a pretty straight forward implementation. The actual project implementation difficulty will depend, however, on whether or not the files uploaded will be read and modified in the backend.
