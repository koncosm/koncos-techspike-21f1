import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { UiService } from 'src/app/services/ui.service';
import { Task } from 'src/app/Task';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
  @Output() onAddFile: EventEmitter<Task> = new EventEmitter();
  shortLink: string = "";
  loading: boolean = false;
  file: File;
  task: {};
  subscription: Subscription;

  constructor(private uiService: UiService) {
    this.subscription = this.uiService
    .onToggle()
    .subscribe((value) => (this.loading = value));
  }

  onChange(event: Event) {
    this.file = (event.target as HTMLInputElement).files![0];
  }

  onUpload() {
    console.log(this.file);

    const fileReader = new FileReader();
    fileReader.readAsText(this.file, "UTF-8");
    fileReader.onload = () => {
      console.log("Beep")
      this.task = (JSON.parse(<string>fileReader.result));
      const newTask = {
        text: (<Task>this.task).text,
        day: (<Task>this.task).day,
        reminder: (<Task>this.task).reminder,
      };
      this.onAddFile.emit(newTask);
    }
    fileReader.onerror = (error) => {
      console.log(error);
    }
    console.log(this.task);
  }

  ngOnInit(): void {
  }

}
